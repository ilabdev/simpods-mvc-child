# Simpods MVC Child
Author: The Simpods Team, Innovation Lab, University of Central Lancashire 
Requires at least: WordPress 4.7
Tested up to: WordPress 5.0.1
Version: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Tags: 

## Description

Simpods MVC Child is a child theme of Simpods MVC.

For more information about Simpods MVC, please go to https://simpodsmvc.me

## Installation

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Simpods MVC in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to https://simpodsmvc.me for a guide on how to use this theme.


## Copyright

Simpods MVC Child WordPress Theme, Copyright 2017 - Present, The Simpods Team, Innovation Lab, University of Central Lancashire 
Simpods MVC Child is distributed under the terms of the GNU GPLv3

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. However, except the components come with the theme, any paid components, modules and add-ons are not under the terms of the GNU General Public License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

## Changelog


Initial release

### 1.0.0
* Released: September 10, 2018