<?php
/**
 * Template Name: Sample Page
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */

get_header();

echo '<main id="main-container" class="container mgt50">';

while ( have_posts() ) : the_post(); 
	apply_filters('the_content', get_the_content() );
endwhile; // end of the loop. 		

echo '</main>';
get_footer(); ?>
