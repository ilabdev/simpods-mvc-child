<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Simpods Sample Front class
 * Copy this file, rename the file and the class name to start a new class
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class MVC_Front_Sample extends MVC_Front_General {

	public function __construct( $init_bln = true ) {

// carry on if it is on the right section
		if ( $init_bln ) {

			$this->actions_fn();
			$this->filters_fn();

		}

	}

	/**
	 * actions_fn wp actions to hook when an instance of the class is created
	 */
	public function actions_fn() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_fn' ) );
	}

	/**
	 * filters_fn wp filters to hook when an instance of the class is created
	 */
	public function filters_fn() {}

	/**
	 * enqueue_fn scripts and styles to enqueue when an instance of the class is created
	 */
	public function enqueue_fn() {}

}