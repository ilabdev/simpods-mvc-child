<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Cross Site Class. You can edit this one.
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class MVC_Front_Cross_Site extends MVC_Front_Parent_Cross_Site {

	public function __construct( $init_bln = true ) {
		$this->actions_fn();
		$this->filters_fn();
	}

	public function actions_fn() {}

	public function filters_fn() {}

	public function enqueue_fn() {}

}
