<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Factory for simpods frontend, collection of static functions and variables
 * for generic purpose, such as processing math or array
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class MVC_Front_Factory extends MVC_Front_Parent_Factory {}
