<?php
if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Simpods Sample Page Controller
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class Ctrl extends Ctrl_MVC_Front_Controller {

	var $hi_str 	= 'Hi, I\'m from the controller';
	var $items_arr	=	array();
	public function __construct() {

		$this->actions_fn();
		$this->filters_fn();
		$this->testing_fn();
	}

	public function actions_fn() {
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_fn' ) );
	}

	public function filters_fn() {}

	public function enqueue_fn() {}

	public function testing_fn(){
		global $funs_cla;
		$this->items_arr = $funs_cla->simpods_select_fn( array(), array( 'target_tb' => 'posts') );
				
	}
}
