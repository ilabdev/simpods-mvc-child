<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Controller cross all sites
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class Cross_Sites_Ctrl extends Ctrl_MVC_Front_Controller {

	public $subtitle_str 	= '';
	public $crossSite_cla	=	'';	

	public function __construct() {
		global $crossSite_cla;

		$this->crossSite_cla	=	$crossSite_cla;
		
		$this->actions_fn();
		$this->filters_fn();
		
	}

	public function actions_fn() {
		
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_fn' ), 9 );

	}

	public function filters_fn() {
	

	}

	public function enqueue_fn() {
	
	}
}

if ( !defined( 'DOING_AJAX' ) ) {
	$GLOBALS['crossCtrl_cla']	=	new Cross_Sites_Ctrl();

} else {

}
