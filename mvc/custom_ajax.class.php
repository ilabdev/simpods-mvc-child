<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Global Ajax class. This file will be available in the frontend and admin areas.
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class Custom_Ajax {

	public function __construct() {

		// logged in users only,
		// add_action( 'wp_ajax_a_method_fn', array( $this, 'a_method_fn' ) );
		// for not logged in users, use wp_ajax_nopriv_ example: add_action( 'wp_ajax_function-name', array( $this, 'function-name') );
		// add_action( 'wp_ajax_nopriv_a_method_fn', array( $this, 'a_method_fn' ) );

	}

	/**
	 * Example ajax function
	 */
	public function a_method_fn() {
		global $wpdb, $current_user, $current_blog, $funs_cla;

		if ( $_POST['action'] != 'a_method_fn' ) {
			wp_die();
		}

		if ( wp_verify_nonce( $_POST['security'], 'whatever' ) === false ) {
			wp_die();
		}

		$a_cla 		= New MVC_Front_Class( false ); // your class
		$return_arr = array(
						'message'	=> 'hello',
						
						);
		wp_send_json_success( $return_arr );
		
	}

}

new Custom_Ajax();
