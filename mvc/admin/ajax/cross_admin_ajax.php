<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Cross Admin Ajax
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date Created
 */
class Cross_Admin_Ajax {

	public function __construct() {

		// logged in users only,
		// add_action( 'wp_ajax_a_method_fn', array( $this, 'a_method_fn' ) );
		// for not logged in users, use wp_ajax_nopriv_ example: add_action( 'wp_ajax_function-name', array( $this, 'function-name') );
		// add_action( 'wp_ajax_nopriv_a_method_fn', array( $this, 'a_method_fn' ) );

	}

	/**
	 * Example ajax function
	 */
	public function a_method_fn() {
		global $wpdb, $current_user, $current_blog, $funs_cla;

		if ( $_POST['action'] != 'a_method_fn' ) {
			wp_die();
		}

		if ( wp_verify_nonce( $_POST['security'], 'whatever' ) === false ) {
			wp_die();
		}

		$sample_cla = New MVC_Simpods_Sample( false );

		$return_arr = array(
						'message'	=> 'hello',
						
						);
		wp_send_json_success( $return_arr );
	}

}

new Cross_Admin_Ajax();
