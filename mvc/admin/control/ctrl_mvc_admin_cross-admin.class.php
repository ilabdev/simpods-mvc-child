<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Simpods MVC Custom Cross admin controller. Users are allowed to modified this file.
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class Ctrl_MVC_Admin_Cross_Admin extends Ctrl_MVC_Admin_Controller {

	public function __construct() {

		$this->actions_fn();
		$this->filters_fn();

	}

	/**
	 * WordPress actions to hook when the admin controller is loaded
	 * @return null
	 */
	public function actions_fn() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_fn' ) );
	}

	/**
	 * WordPress filters to hook when the admin controller is loaded
	 * @return null
	 */
	public function filters_fn() {}

	/**
	 * Scripts & styles to register & enqueue when the admin controller is loaded
	 * @return null
	 */
	public function enqueue_fn() {}

}