<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Sample Admin Controller
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class Ctrl extends Ctrl_MVC_Admin_Controller {

	public function __construct() {

		$this->actions_fn();
		$this->filters_fn();

	}

	/**
	 * WordPress actions to hook when the admin controller is loaded
	 * @return null
	 */
	public function actions_fn() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_fn' ) , 11 );
	}

	/**
	 * WordPress filters to hook when the admin controller is loaded
	 * @return null
	 */
	public function filters_fn() {}

	/**
	 * Scripts & styles to register & enqueue when the admin controller is loaded
	 * Simpods will auto include js and css files with the same "page" value
	 * if the "page" variable is availabe in the URL, if not, it will take the
	 * slug from the current post type or the php file.
	 * @return null
	 */
	public function enqueue_fn() {

		// Prepare Ajax
		wp_localize_script(
			'this-page-js',
			'this_page_ajax',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'nonce'   => wp_create_nonce( 'this_page_ajax' ),
			)
		);
	}

}
