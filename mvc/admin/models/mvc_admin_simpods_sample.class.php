<?php
if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}
/**
 * sample class, duplicate this one when you need to create a new model.
 * Replace sample to the name of your class
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class MVC_Admin_Simpods_Sample extends MVC_Admin_General {

	const page_str  = '';
	const tb_str    = '';
	const title_str = '';

	public $order_int = 10;

	/**
	 * Constructor
	 * @param Boolean $init_bln toggle to run the functions in the constructor,
	 *							$init_bln set to true will run automatically
	 */
	public function __construct( $init_bln = true ) {

		// carry on if it is on the right section
		if ( $init_bln ) {

			$this->actions_fn();
			$this->filters_fn();

		}

	}

	/**
	 * actions_fn wp actions to hook automatically. Global effect.
	 */
	public function actions_fn() {
		//add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_fn' ) , 11 );
	}

	/**
	 * filters_fn wp filters to hook automatically. Global effect.
	 */
	public function filters_fn() {}

	/**
	 * enqueue_fn scripts and styles to enqueue automatically. Global effect.
	 * Simpods will auto include js and css files with the same "page" value 
	 * if the "page" variable is availabe in the URL, if not, it will take the 
	 * slug from the current post type or the php file. 
	 */
	public function enqueue_fn() {}

	/**
	 * to_submenu_fn: add this to the submenu of Simpods
	 */
	public function to_submenu_fn() {}

	/**
	 * interface_fn: add an interface of this
	 */
	public function interface_fn() {}

}