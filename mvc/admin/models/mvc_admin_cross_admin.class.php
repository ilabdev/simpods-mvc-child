<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Cross Admin class
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class MVC_Admin_Cross_Admin extends MVC_Admin_Parent_Cross_Admin {

	/**
	 * Constructor
	 * @param Boolean $init_bln toggle to run the functions in the constructor
	 */
	public function __construct( $init_bln = true ) {

		// carry on if it is on the right section

		if ( $init_bln ) {
			$this->actions_fn();
			$this->filters_fn();
		}

	}

	/**
	 * WordPress actions to hook when the admin controller is loaded
	 * @return null
	 */
	public function actions_fn() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_fn' ) );
	}

	/**
	 * WordPress filters to hook when the admin controller is loaded
	 * @return null
	 */
	public function filters_fn() {}

	/**
	 * Scripts & styles to register & enqueue when the admin controller is loaded
	 * @return null
	 */
	public function enqueue_fn() {}

	/**
	 * Add this page into the submenu of Theme
	 * @return null
	 */
	public function to_submenu_fn() {}

	/**
	 * Interface to display on the appropriate page
	 * @return null
	 */
	public function interface_fn() {}

}
