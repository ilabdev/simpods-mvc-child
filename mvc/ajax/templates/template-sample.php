<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Sample Page Ajax
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class Ajax_Sample {

	public function __construct() {

		// login user only, for everyone, use wp_ajax_nopriv_ example: add_action( 'wp_ajax_function-name', 			array( $this, 'function-name') );
		add_action( 'wp_ajax_functionName_fn', array( $this, 'functionName_fn' ) );
		add_action( 'wp_ajax_functionName_fn', array( $this, 'functionName_fn' ) );

	}

	public function functionName_fn() {
		return '';
	}

}

new Ajax_Sample();