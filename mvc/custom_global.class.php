<?php

if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Custom global settings. You can edit this file
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */

class Custom_Global{

	const page_str  = '';
	const tb_str    = '';
	const title_str = '';
	/**
	 * Constructor
	 * @param Boolean $init_bln toggle to run the functions in the constructor,
	 *							$init_bln set to true will run automatically
	 */
	public function __construct(  ) {
		$this->actions_fn();
	}

	/**
	 * actions_fn wp actions to hook automatically. Global effect.
	 */
	public function actions_fn() { }

	/**
	 * filters_fn wp filters to hook automatically. Global effect.
	 */
	public function filters_fn() {}

	/**
	 * enqueue_fn scripts and styles to enqueue automatically. Global effect.
	 */
	public function enqueue_fn() {}


}
new custom_global();