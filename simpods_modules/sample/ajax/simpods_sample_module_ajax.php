<?php
if ( preg_match( '#' . basename( __FILE__ ) . '#', $_SERVER['PHP_SELF'] ) ) {exit( 'You are not allowed to call this page directly.' );}

/**
 * Cross Site Ajax
 *
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
 */
class Simpods_Sample_Module_Ajax {	


	function __construct() {
			
		// login user only, for everyone, use wp_ajax_nopriv_ example: add_action( 'wp_ajax_function-name', 			array( $this, 'function-name') );				
		add_action( 'wp_ajax_nopriv_test_fn', 		 array( $this, 'test_fn') );
		add_action( 'wp_ajax_test_fn', 		 array( $this, 'test_fn') );		
		//add_action( 'wp_ajax_nopriv_load_jobs_fn',   array( $this, 'load_jobs_fn') );						
				
	}
	
	public function test_fn(){
				
		$return_arr = array(
						'message'	=> 'hello',
						
						);
		wp_send_json_success( $return_arr );
	}
	
}
