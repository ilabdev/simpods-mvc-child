<?php
if ( preg_match('#'.basename(__FILE__).'#', $_SERVER['PHP_SELF']) ) { die('You are not allowed to call this page directly.'); }
/**
* Simpods Sample Module Controller
*
 * @version: 1.0.0
 * @package: Your package
 * @author: Your name
 * @since Date created
*/

class Simpods_Sample_Module_Ctrl {
	
	var $slug_str	=	'simpods_sample_module';
	function __construct( $parent_arr ) {
	
		$this->actions_fn();
		$this->filters_fn();	
		$this->add_shortcodes_fn();		
		
	}
	
    public function actions_fn(){		
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_fn' ) );
	}
	
    public function filters_fn(){			

	}
	/**
	 * The default ones are enqueued by Simpods. This is to enqueued extra files
	 */
    public function enqueue_fn(){
    	global $ctrl_cla; 		
    	//$files_arr	= array( 'arrays' => '/simpods_modules/' . simpods_sample_module . '/extra.js|css' );	
    	//$ctrl_cla->include_fn( $files_arr );	
	}	
	/**
	 * add_shortcodes_fn: add shortcodse 
	 */
	public function add_shortcodes_fn(){
		add_shortcode( $this->slug_str, array( $this, $this->slug_str . '_fn' ) );		
	}
 	 /**
	  * every moudle should create a shortcode to for its frontend view
	  * @param array $atts_arr = array(
			'module_id'  => 0, // module id, simpods will pass in the module id				
	  								  )
	  */
	 public function simpods_sample_module_fn( ){
		global $wpdb, $current_user,$ctrl_cla, $crossSite_cla;
		
		$modHolder_arr 	= $ctrl_cla->modHolder_arr;	//the data of the module items		

		$atts_arr = shortcode_atts( array(
									'module_id' => 0,				
									), $modHolder_arr );		
		
		$children_arr 	= $ctrl_cla->modItems_arr;	//the data of the module items		
		$mod_cla 		= new $this->slug_str();		//the module's default class		
	 }	

}
